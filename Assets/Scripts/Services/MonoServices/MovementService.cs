﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class MovementService : MonoBehaviour {

	public static MovementService Instance;

	[SerializeField] private float gravitation = -10f;
	[SerializeField] private float horizontalSlick = .9f;
	
	[SerializeField] private float radius = 10;
	
	private List<PlayerDataModel> _models;
	private Vector2 centerPivotPosition;
	
	void Awake()
	{
		Instance = this;
		_models = new List<PlayerDataModel>();
	}
	
	void Update () {
		foreach (var model in _models)
		{
			model.anglePosition += model.HorizontalSpeed * Time.deltaTime;
			
			model.Transform.localRotation = Quaternion.Euler(0,- model.anglePosition * Mathf.Rad2Deg,0);
			model.Transform.localPosition = new Vector3(radius * Mathf.Cos(model.anglePosition)
														, model.Transform.localPosition.y + (model.OnGround ? 0 : model.VerticalSpeed * Time.deltaTime)
														, radius * Mathf.Sin(model.anglePosition));
			
			model.HorizontalSpeed *= horizontalSlick;
			model.VerticalSpeed += model.OnGround ? 0 : model.weight * gravitation * Time.deltaTime;
		}
	}

	public void AddMovementPlayer(PlayerDataModel model)
	{
		_models.Add(model);
	}
	
	public void RemoveMovementPlayer(PlayerDataModel model)
	{
		_models.Remove(model);
	}
}
