﻿using UnityEngine;

[System.Serializable]
public class PlayerDataModel
{
    public float anglePosition = Mathf.PI / -2;
    public float HorizontalSpeed;
    public float VerticalSpeed;
    public bool OnGround;
    public bool Jumped;
    public bool DoubleJumped;
    public Transform Transform;
    public float weight = 1;
}
