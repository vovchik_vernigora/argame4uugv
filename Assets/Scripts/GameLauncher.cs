﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.XR;
using UnityEngine.SpatialTracking;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;

public class GameLauncher : MonoBehaviour
{
	public ARSessionOrigin ArSessionOrigin;
	public ARPlaneManager planeManager;

	public Transform gameLevel;

	private bool inited;
	
	void Update ()
	{
		if (inited)
		{
			return;
		}

		if (Input.touchCount == 0)
		{
			return;
		}

		if (Input.touchCount == 1)
		{
			List<ARRaycastHit> hitResults = new List<ARRaycastHit>();
			ArSessionOrigin.Raycast(Input.GetTouch(0).position, hitResults, TrackableType.All);
			if (hitResults.Count > 0)
			{
				gameLevel.transform.position = hitResults[0].pose.position;
			}

			inited = true;
			
			var planes = new List<ARPlane>();
			planeManager.GetAllPlanes(planes);
			planeManager.enabled = false;
			foreach (var p in planes)
			{
				Destroy(p);
			}
		}
	}
}
