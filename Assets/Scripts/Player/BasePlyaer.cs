﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters;
using UnityEngine;

public class BasePlyaer : MonoBehaviour
{
	[SerializeField] private PlayerDataModel dataModel;
	
	void Start () {
		
		dataModel = new PlayerDataModel() {Transform = transform, weight = 2};
		
		InputService.Instance.AssignHorizontalInput(AddHorizaontalMovement);
		InputService.Instance.AssignVerticalInput(AddVerticalMovement);
		
		MovementService.Instance.AddMovementPlayer(dataModel);
	}

	void OnDestroy()
	{
		InputService.Instance.UnassignHorizontalInput(AddHorizaontalMovement);
		InputService.Instance.UnassignVerticalInput(AddVerticalMovement);
		
		MovementService.Instance.RemoveMovementPlayer(dataModel);
	}

	void OnTriggerEnter(Collider other)
	{
		CollisionService.TriggerEnter(other, ref dataModel);
	}

	void OnTriggerExit(Collider other)
	{
		CollisionService.TriggerExit(other, ref dataModel);
	}
	
	private void AddHorizaontalMovement(float horizontalDirection)
	{
		dataModel.HorizontalSpeed = horizontalDirection;
	}
	
	private void AddVerticalMovement(float verticalForce)
	{
		if (dataModel.DoubleJumped)
		{
			return;
		}

		if (dataModel.Jumped)
		{
			dataModel.DoubleJumped = true;
		}
		
		dataModel.VerticalSpeed = verticalForce;
		dataModel.OnGround = false;
		dataModel.Jumped = true;
	}	
}
