﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputService : MonoBehaviour
{
	public static InputService Instance;
	
	[SerializeField] private float jumpSpeed = 10;
	[SerializeField] private float horizontalCoef = 10;

	
	private event Action<float> horizontalMovementUpdate;
	private event Action<float> verticalMovementUpdate;

	void Awake()
	{
		Instance = this;
	}

	void Update ()
	{
		float horizontalAxis = Input.GetAxis("Horizontal");
		if (Mathf.Abs(horizontalAxis) > 0.01)
		{
			if (horizontalMovementUpdate != null)
			{
				horizontalMovementUpdate(horizontalAxis * horizontalCoef);
			}
		}
		
		if (Input.GetKeyDown(KeyCode.Space))
		{
			if (verticalMovementUpdate != null)
			{
				verticalMovementUpdate(jumpSpeed);
			}
		}
	}

	/// <summary>
	/// Temp UI
	/// </summary>
	void OnGUI()
	{
		if (GUI.Button(new Rect(0,Screen.height / 6f * 5,Screen.width / 3f, Screen.height / 6f), "<"))
		{
			if (horizontalMovementUpdate != null)
			{
				horizontalMovementUpdate(-.5f * horizontalCoef);
			}
		}
		
		if (GUI.Button(new Rect(Screen.width / 3f * 2, Screen.height / 6f * 5, Screen.width / 3f, Screen.height / 5f), ">"))
		{
			if (horizontalMovementUpdate != null)
			{
				horizontalMovementUpdate(.5f * horizontalCoef);
			}
		}
		if (GUI.Button(new Rect(Screen.width / 3f, Screen.height / 6f * 5, Screen.width / 3f, Screen.height / 5f), "Jump"))
		{
			if (verticalMovementUpdate != null)
			{
				verticalMovementUpdate(jumpSpeed);
			}
		}
	}

	public void AssignHorizontalInput(Action<float> delegateAction)
	{
		horizontalMovementUpdate += delegateAction;
	}
	
	public void UnassignHorizontalInput(Action<float> delegateAction)
	{
		horizontalMovementUpdate -= delegateAction;
	}
	
	public void AssignVerticalInput(Action<float> delegateAction)
	{
		verticalMovementUpdate += delegateAction;
	}
	
	public void UnassignVerticalInput(Action<float> delegateAction)
	{
		verticalMovementUpdate -= delegateAction;
	}
}
