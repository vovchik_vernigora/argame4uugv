﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseCollisionProcessor
{
	public abstract void TriggerEnter(ref PlayerDataModel dataModel);
	public abstract void TriggerExit(ref PlayerDataModel dataModel);
}

public class DefaultGroundCollisionProcessor : BaseCollisionProcessor
{
	public float SpeedToJumpOnColision = 5f;
	public float SpeedSafePersantageOnJumpOnColision = -.4f;
	
	public override void TriggerEnter(ref PlayerDataModel dataModel)
	{
		if (Mathf.Abs(dataModel.VerticalSpeed) > SpeedToJumpOnColision)
		{
			dataModel.VerticalSpeed *= SpeedSafePersantageOnJumpOnColision;
		}
		else
		{
			dataModel.VerticalSpeed = 0;
			dataModel.OnGround = true;
		}
		dataModel.DoubleJumped = false;
		dataModel.Jumped = false;
	}

	public override void TriggerExit(ref PlayerDataModel dataModel)
	{
		dataModel.OnGround = false;
	}
}

public class VerticalBlockGroundCollisionProcessor : BaseCollisionProcessor
{
	public float RevertSpeed = -3f;
	
	public override void TriggerEnter(ref PlayerDataModel dataModel)
	{
		dataModel.HorizontalSpeed *= RevertSpeed;
	}

	public override void TriggerExit(ref PlayerDataModel dataModel)
	{
	}
}

public static class CollisionService {

	private static readonly Dictionary<string, BaseCollisionProcessor> Processors = new Dictionary<string, BaseCollisionProcessor>();

	static CollisionService()
	{
		Processors.Add("GroundDefault", new DefaultGroundCollisionProcessor());
		Processors.Add("GroundVertical", new VerticalBlockGroundCollisionProcessor());
	}

	public static void TriggerEnter(Collider other, ref PlayerDataModel dataModel)
	{
		BaseCollisionProcessor processor = null;
		if (Processors.TryGetValue(other.tag, out processor))
		{
			processor.TriggerEnter(ref dataModel);
		}
		else
		{
			Debug.LogWarningFormat("No processor for tag [{0}]", other.tag);
		}
	}

	public static void TriggerExit(Collider other, ref PlayerDataModel dataModel)
	{
		BaseCollisionProcessor processor = null;
		if (Processors.TryGetValue(other.tag, out processor))
		{
			processor.TriggerExit(ref dataModel);
		}
		else
		{
			Debug.LogWarningFormat("No processor for tag [{0}]", other.tag);
		}
	}
}
