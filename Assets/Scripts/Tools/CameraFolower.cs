﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class CameraFolower : MonoBehaviour
{
	[SerializeField] private Transform trToFolow;
	[SerializeField] private float moveCoef = 0.3f;

	void Awake()
	{
		Assert.IsNotNull(trToFolow);
	}

	void Update () {
		transform.Translate(0, (trToFolow.localPosition.y - transform.localPosition.y) * moveCoef,0, Space.World);
	}
}
